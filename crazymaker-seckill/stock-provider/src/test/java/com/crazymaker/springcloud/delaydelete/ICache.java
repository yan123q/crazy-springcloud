package com.crazymaker.springcloud.delaydelete;

public interface ICache {
    boolean isRunning();

    void delete(String t);
}

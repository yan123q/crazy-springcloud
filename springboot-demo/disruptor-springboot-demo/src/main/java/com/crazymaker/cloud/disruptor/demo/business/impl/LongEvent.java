package com.crazymaker.cloud.disruptor.demo.business.impl;

public class LongEvent {
    private long value;
    public long getValue() { 
        return value; 
    } 
 
    public void setValue(long value) { 
        this.value = value; 
    } 
} 